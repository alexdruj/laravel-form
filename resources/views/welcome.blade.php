<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="_token" content="{{csrf_token()}}" />

        <title>Бэкэнд на PHP для веб-формы</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <!-- Styles -->

        <style type="text/css">
            .dc-form {
                display: block;
                margin: 300px auto;
                background-color: #BF69BC;
                max-width: 400px;
                border-radius: 5px;
                box-shadow: 0 0 20px rgba(0, 0, 0, 0.3);
                border-radius: 10px;
            }
            .dc-form form {
                padding: 10px;
            }
            .dc-form h2{
                color: #fff;
                text-align: center;
                font-family: 'Open Sans', sans-serif;
                text-shadow: 0 0 20px rgba(0, 0, 0, 0.3);
            }
            .dc-form input, textarea {
                display: block;
                margin: auto;
                padding: 8px;
                margin-bottom: 5px;
                border-radius: 5px;
                border: 0px;
                width: 200px;
                max-width: 200px;
                min-width: 200px;
            }
            .dc-form #form-button {
                display: block;
                margin: auto;
                padding: 5px;
                margin-bottom: 5px;
                margin-top: 13px;
                border-radius: 50px;
                border: 0px;
                width: 100px;
                background-color: #f7d9f6;
                color: #333;
            }
            .dc-form #form-button:hover {
                background-color: #fff;
                transition: background-color 0.3s ease-in-out;
            }
            .alert-danger p {
                text-align: center;
                color: #fff;
                background-color: #d01b3e;
                border-radius: 30px;
                padding: 5px 0;
                margin: 5px 0 20px 0;
                font-family: 'Open Sans', sans-serif;
                font-weight: 600;
                font-size: 15px;
            }
            .form-success {
                text-align: center;
                color: #fff;
                background-color: #3b7756;
                border-radius: 30px;
                padding: 10px 0;
                margin: 5px 0 20px 0;
                font-family: 'Open Sans', sans-serif;
                font-weight: 600;
                font-size: 15px;
            }
        </style>
    </head>
    <body>
        <div class="dc-form">
            <form method="POST" id="form">
                {{ csrf_field() }}
                <h2>Форма обратной связи</h2>
                <div class="alert alert-danger" style="display:none"></div>
                <div class="form-success" style="display:none"></div>
                <input type="email" name="email" placeholder="email*" >
                <input type="tel" name="phone" placeholder="номер телефона*" >
                <textarea placeholder="сообщение" name="message" rows="5"></textarea>
                <button id="form-button" type="submit">Отправить</button>
            </form>
        </div>
    </body>
</html>

<script>
    $(document).ready(function(){
        $('#form').on('submit', function(e){
            e.preventDefault();
            $.ajax({
                type: 'POST',
                url: '{{route('form')}}',
                data: $('#form').serialize(),
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(data){
                    if (data.success) {
                        console.log(data);
                        $('input[name="email"]').val('');
                        $('input[name="phone"]').val('');
                        $('textarea[name="message"]').val('');
                        $('.alert-danger').contents('p').remove();
                        $('.form-success').html(data.success).show().fadeOut(8000);
                    } else {
                        console.log('error!');
                        $.each(data.errors, function(key, value) {
                            $('.alert-danger').contents('p').remove();
                            $('.alert-danger').show();
                            $('.alert-danger').append('<p>' + value + '</p>');
                        });
                    }
                },
            });
        });
    });
</script>