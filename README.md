## Конфигурация

#### **База данных**

Импортируйте файл `dc.sql` в вашу СУБД.

В файле конфигурации `.env` пропишите ваши параметры соединения с базой данных.

```
DB_HOST=[хост]
DB_PORT=[порт]
DB_DATABASE=[название базы данных]
DB_USERNAME=[имя пользователя]
DB_PASSWORD=[пароль пользователя]
```

## Требования к окружению

- **PHP** >= 7.1
- **PDO** расширение для PHP (для версии 5.1+)
- **MCrypt** расширение для PHP (для версии 5.0)
- **OpenSSL** (расширение для PHP)
- **Mbstring** (расширение для PHP)
- **Tokenizer** (расширение для PHP)
- **XML** (расширение для PHP) (для версии 5.3+)

## Установка

1. Установить [composer](https://getcomposer.org/download/) 
2. Установить пакеты `composer install` или `php composer.phar install`
3. Заполнить настройки базы данных в файле `.env`
4. Для локального запуска необходимо запустить встроенный сервер командой `php artisan serve`

## Описание

#### **Формат принимаемых данных**

- JSON

Содержит в себе:

- CSRF-токен
- Электронная почта
- Номер телефона
- Текст сообщения

#### **Формат ответа**

- JSON

#### **Примеры входящих запросов и ответов на них**

Входящий запрос

```json
{
    "_token" : "ZNsnw0Vh8MZ6yc3eDPJjIKrLUl2wYwTsi04F2iEj",
    "email" : "test@test.com",
    "phone" : "89995555666",
    "message" : "test"
}
```

Ответ на запрос в случае успеха

```json
{
    "success" : "Данные обработаны успешно"
}
```

Ответ на запрос в случае неудачи

```json
{
    "errors" : "Произошла ошибка записи данных в бд"
}
```