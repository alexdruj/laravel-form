$(document).ready(function(){
    $('#form').on('submit', function(e){
        e.preventDefault();

        $.ajax({
            type: 'POST',
            url: '/form',
            data: $('#form').serialize(),
            success: function(result){
                console.log(result);
                $('#form-button').disable;
            }
        });
    });
});