<?php

namespace App\Http\Controllers\Ajax;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class FormController extends Controller
{

    public function send(Request $request)
    {

            //присвоение типов проверки и установка своих описаний ошибок
            $validator = Validator::make($request->all(), [
                'email' => 'required|email',
                'phone' => 'required|digits_between:10,12',
            ], [
                'email.required' => 'Поле email обязательно к заполнению',
                'phone.required' => 'Поле телефон обязательно к заполнению',
                'phone.digits_between' => 'Телефон должен состоять из от 10 до 12 цифр',
            ]);

            //проверка прохождения валидации. Возвращение ошибок при неудачи
            if ($validator->fails())
            {
                return response()->json(['errors'=>$validator->errors()->all()]);
            }

        //вставка значений в бд
        $data = array(
            'id' => NULL,
            'email' => $request['email'],
            'phone' => $request['phone'],
            'message' => $request['message']);
        $result = DB::table('form')->insert($data);

        //отправка сообщения
        if ($result == true) return response()->json(['success'=>'Данные обработаны успешно']);
        else return response()->json(['errors'=>'Произошла ошибка записи данных в бд']);

    }
}
