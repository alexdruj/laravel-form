-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Мар 01 2019 г., 01:21
-- Версия сервера: 5.6.38
-- Версия PHP: 7.1.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `dc`
--

-- --------------------------------------------------------

--
-- Структура таблицы `form`
--

CREATE TABLE `form` (
  `id` int(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `phone` varchar(13) NOT NULL,
  `message` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `form`
--

INSERT INTO `form` (`id`, `email`, `phone`, `message`) VALUES
(1, 'test@test.com', '88888888888', 'gfhfh'),
(2, 'test@test.com', '88888888888', 'gfhfh'),
(3, 'test@test.com', '88888888888', 'gfhfh'),
(4, 'test@test.com', '4566556543', 'nn'),
(5, 'test@test1.com', '67895478904', 'Сообщение'),
(6, 'test@test2.com', '95445612354', 'Еще одна запись'),
(7, 'test@test5.com', '67566743987', NULL),
(8, 'test@test5.com', '67566743987', NULL),
(9, 'test@test12.com', '67899876576', 'ТЕСТ'),
(10, 'test@test1178.com', '78971245651', 'test'),
(11, 'test@test3.com', '75134587951', '1'),
(12, 'test@test114.com', '56', NULL),
(13, 'test@test.com', '32443556789', NULL),
(14, 'test@test.com', '45676784563', NULL),
(15, 'test@test.com', '23345623456', NULL),
(16, 'test@test.com', '34565464546', 'vv'),
(17, 'test@test.com', '21567890212', 'ttkk'),
(18, 'test@test.com', '454567890756', NULL),
(19, 'test@test.com', '678768456786', 'HI');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `form`
--
ALTER TABLE `form`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `form`
--
ALTER TABLE `form`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
